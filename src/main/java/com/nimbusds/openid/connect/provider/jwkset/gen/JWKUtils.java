package com.nimbusds.openid.connect.provider.jwkset.gen;


import com.nimbusds.jose.jwk.*;

import java.util.LinkedList;
import java.util.List;


/**
 * JWK utilities.
 */
public class JWKUtils {


        /**
         * Creates a matcher for the purpose of the specified JWK.
         *
         * @param jwk The JWK.
         *
         * @return The JWK matcher.
         */
        public static JWKMatcher createPurposeMatcher(final JWK jwk) {

                var builder = new JWKMatcher.Builder()
                        .keyType(jwk.getKeyType())
                        .keySize(KeyType.RSA.equals(jwk.getKeyType()) ? 0 : jwk.size()) // For RSA match any size
                        .privateOnly(true)
                        .keyUse(jwk.getKeyUse())
                        .withKeyIDOnly(jwk.getKeyID() != null);

                if (jwk instanceof ECKey) {
                        builder = builder.curve(jwk.toECKey().getCurve());

                } else if (jwk instanceof OctetKeyPair) {
                        builder = builder.curve(jwk.toOctetKeyPair().getCurve());
                }

                return builder.build();
        }


        /**
         * Revokes all matching JWKs in the specified set.
         *
         * @param jwkSet        The JWK set.
         * @param jwkMatcher    The JWK matcher.
         * @param keyRevocation The key revocation.
         *
         * @return The resulting JWK set.
         */
        public static JWKSet revokeMatchingJWKs(final JWKSet jwkSet,
                                                final JWKMatcher jwkMatcher,
                                                final KeyRevocation keyRevocation) {

                List<JWK> keys = new LinkedList<>();

                for (JWK jwk : jwkSet.getKeys()) {

                        if (jwkMatcher.matches(jwk) && jwk.getKeyRevocation() == null) {
                                keys.add(jwk.toRevokedJWK(keyRevocation));
                        } else {
                                keys.add(jwk);
                        }
                }

                return new JWKSet(keys);
        }


        /**
         * Inserts the specified keys at the beginning of a JWK set.
         *
         * @param jwkSet The JWK set.
         * @param keys   The keys to insert.
         *
         * @return The updated JWK set.
         */
        static JWKSet prefixKeys(final JWKSet jwkSet, final List<JWK> keys) {

                List<JWK> updatedKeyList = new LinkedList<>();
                updatedKeyList.addAll(keys);
                updatedKeyList.addAll(jwkSet.getKeys());
                return new JWKSet(updatedKeyList);
        }


        /**
         * Appends the specified keys at the end of a JWK set.
         *
         * @param jwkSet The JWK set.
         * @param keys   The keys to append.
         *
         * @return The updated JWK set.
         */
        static JWKSet postfixKeys(final JWKSet jwkSet, final List<JWK> keys) {

                List<JWK> updatedKeyList = new LinkedList<>();
                updatedKeyList.addAll(jwkSet.getKeys());
                updatedKeyList.addAll(keys);
                return new JWKSet(updatedKeyList);
        }


        private JWKUtils() {}
}
