package com.nimbusds.openid.connect.provider.jwkset.gen;


import java.util.LinkedHashSet;

import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import org.apache.commons.lang3.RandomStringUtils;


/**
 * Ordered key ID set.
 */
public class KeyIDs extends LinkedHashSet<String> {
	
	
	/**
	 * Creates a new empty key ID set.
	 */
	public KeyIDs() {
		
	}
	
	
	/**
	 * Creates a new key ID set from the key IDs found in the specified
	 * JWK set.
	 *
	 * @param jwkSet The JWK set.
	 */
	public KeyIDs(final JWKSet jwkSet) {
		
		for (JWK jwk: jwkSet.getKeys()) {
			
			if (jwk.getKeyID() != null) {
				add(jwk.getKeyID());
			}
		}
	}
	
	
	/**
	 * Generates a new random unique key ID and adds it to this set.
	 *
	 * @return The added random unique key ID.
	 */
	public String addRandomUniqueKeyID() {
		
		while (true) {
			
			String kid = RandomStringUtils.randomAlphanumeric(4);
			
			if (! contains(kid)) {
				add(kid);
				return kid;
			}
		}
	}
}
