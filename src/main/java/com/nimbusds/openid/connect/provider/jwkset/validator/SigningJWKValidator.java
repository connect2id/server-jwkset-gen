package com.nimbusds.openid.connect.provider.jwkset.validator;


import java.security.Provider;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.*;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.ECKey;
import com.nimbusds.jose.jwk.OctetKeyPair;
import com.nimbusds.jose.jwk.RSAKey;
import org.checkerframework.checker.nullness.qual.Nullable;


/**
 * Validates a signing JWK by performing a test sign and verify.
 */
public class SigningJWKValidator {
	
	
	private static JWSObject createTestJWSObject(final JWSAlgorithm alg) {
		
		var header = new JWSHeader(alg);
		var payload = new Payload("test");
		return new JWSObject(header, payload);
	}


	/**
	 * Tests signing and verification with the specified RSA JWK.
	 *
	 * @param jwk            The RSA JWK. Must contain a private part.
	 * @param signProvider   The JCA provider for the sign operation,
	 *                       {@code null} to use the default one.
	 * @param verifyProvider The JCA provider for the verify operation,
	 *                       {@code null} to use the default one.
	 *
	 * @throws JOSEException If the RSA key is deemed invalid.
	 */
	public static void testSignAndVerify(final RSAKey jwk,
					     final @Nullable Provider signProvider,
					     final @Nullable Provider verifyProvider)
		throws JOSEException {

		if (! jwk.isPrivate())
			throw new JOSEException("The RSA JWK (kid=" + jwk.getKeyID() + ") has no private part");

		try {
			// Create and sign JWS with private RSA key
			JWSSigner signer = new RSASSASigner(jwk.toPrivateKey());
			
			if (signProvider != null) {
				signer.getJCAContext().setProvider(signProvider);
			}
			
			JWSObject jwsObject = createTestJWSObject(JWSAlgorithm.RS256);
			jwsObject.sign(signer);

			// Verify with public RSA key
			JWSVerifier verifier = new RSASSAVerifier(jwk);
			
			if (verifyProvider != null) {
				verifier.getJCAContext().setProvider(verifyProvider);
			}
			
			if (! jwsObject.verify(verifier)) {
				throw new JOSEException("Test RSA JWK (kid=" + jwk.getKeyID() + ") signature verification failed");
			}

		} catch (Exception e) {
			throw new JOSEException("RSA JWK (kid=" + jwk.getKeyID() + ") validation failed: " + e.getMessage(), e);
		}
	}
	
	
	/**
	 * Tests signing and verification with the specified EC JWK.
	 *
	 * @param jwk            The EC JWK. Must contain a private part.
	 * @param signProvider   The JCA provider for the sign operation,
	 *                       {@code null} to use the default one.
	 * @param verifyProvider The JCA provider for the verify operation,
	 *                       {@code null} to use the default one.
	 *
	 * @throws JOSEException If the RSA key is deemed invalid.
	 */
	public static void testSignAndVerify(final ECKey jwk,
					     final @Nullable Provider signProvider,
					     final @Nullable Provider verifyProvider)
		throws JOSEException {
		
		if (! jwk.isPrivate())
			throw new JOSEException("The EC JWK (crv=" + jwk.getCurve() + " kid=" + jwk.getKeyID() + ") has no private part");

		try {
			// Create and sign JWS with private EC key
			JWSSigner signer = new ECDSASigner(jwk.toPrivateKey(), jwk.getCurve());
			
			if (signProvider != null) {
				signer.getJCAContext().setProvider(signProvider);
			}
			
			final JWSAlgorithm alg;
			if (Curve.P_256.equals(jwk.getCurve())) {
				alg = JWSAlgorithm.ES256;
			} else if (Curve.P_384.equals(jwk.getCurve())) {
				alg = JWSAlgorithm.ES384;
			} else if (Curve.P_521.equals(jwk.getCurve())) {
				alg = JWSAlgorithm.ES512;
			} else if (Curve.SECP256K1.equals(jwk.getCurve())) {
				alg = JWSAlgorithm.ES256K;
			} else {
				throw new JOSEException("Unsupported EC JWK (kid=" + jwk.getKeyID() + ") curve: " + jwk.getCurve());
			}
			
			JWSObject jwsObject = createTestJWSObject(alg);
			jwsObject.sign(signer);

			// Verify with public EC key
			JWSVerifier verifier = new ECDSAVerifier(jwk);
			
			if (verifyProvider != null) {
				verifier.getJCAContext().setProvider(verifyProvider);
			}
			
			if (! jwsObject.verify(verifier)) {
				throw new JOSEException("Test EC JWK (crv=" + jwk.getCurve() + " kid=" + jwk.getKeyID() + ") signature verification failed");
			}

		} catch (Exception e) {
			throw new JOSEException("EC JWK (crv=" + jwk.getCurve() + " kid=" + jwk.getKeyID() + ") validation failed: " + e.getMessage(), e);
		}
	}
	
	
	/**
	 * Tests signing and verification with the specified Ed25519 JWK.
	 *
	 * @param jwk The Ed25519 JWK. Must contain a private part.
	 *
	 * @throws JOSEException If the OKP key is deemed invalid.
	 */
	public static void testSignAndVerify(final OctetKeyPair jwk)
		throws JOSEException {
		
		if (! jwk.isPrivate())
			throw new JOSEException("The OKP JWK (kid=" + jwk.getKeyID() + ") has not private part");
		
		try {
			JWSSigner signer = new Ed25519Signer(jwk);
			JWSObject jwsObject = createTestJWSObject(JWSAlgorithm.EdDSA);
			jwsObject.sign(signer);
			
			// Verify with public EC key
			if (! jwsObject.verify(new Ed25519Verifier(jwk.toPublicJWK()))) {
				throw new JOSEException("Test Ed25519 JWK (kid=" + jwk.getKeyID() + ") signature verification failed");
			}
		} catch (Exception e) {
			throw new JOSEException("Ed25519 JWK (kid=" + jwk.getKeyID() + ") validation failed: " + e.getMessage(), e);
		}
	}
}
