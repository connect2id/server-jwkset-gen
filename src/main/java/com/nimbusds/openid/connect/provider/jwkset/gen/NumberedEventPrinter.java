package com.nimbusds.openid.connect.provider.jwkset.gen;


/**
 * Numbered STDOUT printer for event messages.
 */
public class NumberedEventPrinter {
	
	
	/**
	 * The current event number.
	 */
	private int eventNumber = 1;
	
	
	/**
	 * Prints the specified event message to STDOUT.
	 *
	 * @param eventMessage The event message, {@code null} if nothing to
	 *                     print.
	 */
	public void print(final String eventMessage) {
		
		if (eventMessage == null) {
			return;
		}
		
		System.out.println("[" + (eventNumber++) + "] " + eventMessage);
	}
}
