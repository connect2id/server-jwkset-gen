package com.nimbusds.openid.connect.provider.jwkset.validator;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKMatcher;
import com.nimbusds.jose.jwk.JWKSelector;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;


/**
 * Connect2id server JWKs validator.
 */
public final class JWKsValidator {


	/**
	 * OpenID provider / OAuth 2.0 authorisation server JWKs validator.
	 */
	public static class OP {
	
	
		/**
		 * Validates that the minimum required keys are present in the
		 * specified the JWK set.
		 *
		 * <ul>
		 *     <li>One signing RSA key pair
		 *     <li>One HMAC key
		 *     <li>One subject encryption AES key
		 *     <li>One refresh token encryption AES key
		 * </ul>
		 *
		 * @see JWKsSpec
		 *
		 * @param jwkSet The JWK set to validate.
		 *
		 * @throws JOSEException If validation failed.
		 */
		public static void validatePresenceOfMinimumRequiredKeys(final JWKSet jwkSet)
			throws JOSEException {

			// RSA signing key
			JWKMatcher rsaSigningKeyMatcher = new JWKMatcher.Builder(JWKsSpec.OP.RotatedRSASigning.createKeyMatcher(JWSAlgorithm.RS256))
				.nonRevokedOnly(true)
				.build();

			if (new JWKSelector(JWKsSpec.OP.RotatedRSASigning.createKeyMatcher(JWSAlgorithm.RS256)).select(jwkSet).isEmpty()) {
				throw new JOSEException("The JWK set must contain at least one signing RSA key pair with properties: " + rsaSigningKeyMatcher);
			}

			// HMAC key
			JWKMatcher hmacMatcher = new JWKMatcher.Builder(JWKsSpec.OP.HMAC.KEY_MATCHER)
				.nonRevokedOnly(true)
				.build();

			if (new JWKSelector(hmacMatcher).select(jwkSet).isEmpty()) {
				throw new JOSEException("The JWK set must contain one HMAC secret key with properties: " + hmacMatcher);
			}

			// Subject encryption (pairwise) key
			JWKMatcher subjectEncryptionKeyMatcher = new JWKMatcher.Builder(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER)
				.nonRevokedOnly(true)
				.build();

			if (new JWKSelector(subjectEncryptionKeyMatcher).select(jwkSet).isEmpty()) {
				throw new JOSEException("The JWK set must contain one subject encryption AES key with properties: " + subjectEncryptionKeyMatcher);
			}

			// Refresh token encryption key
			JWKMatcher refreshTokenEncryptionKeyMatcher = new JWKMatcher.Builder(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER)
				.nonRevokedOnly(true)
				.build();

			if (new JWKSelector(refreshTokenEncryptionKeyMatcher).select(jwkSet).isEmpty()) {
				throw new JOSEException("The JWK set must contain one refresh token encryption AES key with properties: " + refreshTokenEncryptionKeyMatcher);
			}
		}
	}


	/**
	 * OpenID Federation 1.0 entity JWKs validator.
	 */
	public static class Federation {

		/**
		 * Validates that the minimum required keys for an OpenID
		 * Federation 1.0 entity are present in the specified JWK set.
		 *
		 * <ul>
		 *     <li>One signing RSA key pair
		 * </ul>
		 *
		 * @see JWKsSpec
		 *
		 * @param jwkSet The JWK set to validate.
		 *
		 * @throws JOSEException If validation failed.
		 */
		public static void validatePresenceOfMinimumRequiredKeys(final JWKSet jwkSet)
			throws JOSEException {

			JWKMatcher rsaSigningKeyMatcher = JWKsSpec.Federation.RotatedRSASigning.createKeyMatcher(JWSAlgorithm.RS256);

			if (new JWKSelector(rsaSigningKeyMatcher).select(jwkSet).isEmpty()) {
				throw new JOSEException("The JWK set must contain at least one signing RSA key pair with properties: " + rsaSigningKeyMatcher);
			}
		}
	}
}
