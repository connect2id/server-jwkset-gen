package com.nimbusds.openid.connect.provider.jwkset.gen;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;
import org.checkerframework.checker.nullness.qual.Nullable;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;


/**
 * Connect2id server JWKs generator.
 */
public class JWKsGenerator {


	/**
	 * Key store JWK generator.
	 */
	public static class KeyStore {


		/**
		 * Generates a new encryption JWK.
		 *
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The JWK.
		 */
		public static JWK generate(final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			var keyIDs = new KeyIDs();

			JWK jwk = JWKsSpec.KeyStore.generateKey(keyIDs.addRandomUniqueKeyID());

			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new key store encryption AES " + jwk.size() + " bit key with ID " + jwk.getKeyID());
			}

			return jwk;
		}
	}


	/**
	 * OpenID provider / OAuth 2.0 authorisation server JWKs generator.
	 */
	public static class OP {


		/**
		 * Generates a new set of rotating signing and encryption keys.
		 *
		 * @param reservedKeyIDs   The reserved key IDs, empty if none.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The generated rotating keys.
		 */
		public static List<JWK> generateRotatingKeys(final KeyIDs reservedKeyIDs,
							     final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			return generateRotatingKeys(reservedKeyIDs, JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE, false, eventMessageSink);
		}


		/**
		 * Generates a new set of rotating signing and encryption keys.
		 *
		 * @param reservedKeyIDs   The reserved key IDs, empty if none.
		 * @param rsaKeyBitSize    The RSA key bit size. Must be one of
		 *                         {@link JWKsSpec#SUPPORTED_RSA_KEY_BIT_SIZES}.
		 * @param noEdDSA          {@code true} to skip the EdDSA key
		 *                         generation.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The generated rotating keys.
		 */
		public static List<JWK> generateRotatingKeys(final KeyIDs reservedKeyIDs,
						             final int rsaKeyBitSize,
							     final boolean noEdDSA,
						             final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			List<JWK> keys = new LinkedList<>();

			var keyIDs = new KeyIDs();
			keyIDs.addAll(reservedKeyIDs);

			RSAKey rsaKey = JWKsSpec.OP.RotatedRSASigning.generateKey(keyIDs.addRandomUniqueKeyID(), rsaKeyBitSize);
			keys.add(rsaKey);
			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new signing RSA " + rsaKeyBitSize + " bit key with ID " + rsaKey.getKeyID());
			}

			for (Curve crv: JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES) {
				ECKey ecKey = JWKsSpec.OP.RotatedECSigning.generateKey(crv, keyIDs.addRandomUniqueKeyID());
				keys.add(ecKey);
				if (eventMessageSink != null) {
					eventMessageSink.accept("Generated new signing EC " + ecKey.getCurve() + " key with ID " + ecKey.getKeyID());
				}
			}

			if (! noEdDSA) {
				OctetKeyPair okp = JWKsSpec.OP.RotatedEdDSASigning.generateKey(keyIDs.addRandomUniqueKeyID());
				keys.add(okp);
				if (eventMessageSink != null) {
					eventMessageSink.accept("Generated new signing " + okp.getCurve() + " key with ID " + okp.getKeyID());
				}
			}

			rsaKey = JWKsSpec.OP.RotatedRSAEncryption.generateKey(keyIDs.addRandomUniqueKeyID(), rsaKeyBitSize);
			keys.add(rsaKey);
			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new encryption RSA " + rsaKeyBitSize + " bit key with ID " + rsaKey.getKeyID());
			}

			for (Curve crv: JWKsSpec.OP.RotatedECDHEncryption.SUPPORTED_CURVES) {
				ECKey ecKey = JWKsSpec.OP.RotatedECDHEncryption.generateKey(crv, keyIDs.addRandomUniqueKeyID());
				keys.add(ecKey);
				if (eventMessageSink != null) {
					eventMessageSink.accept("Generated new encryption EC " + ecKey.getCurve() + " key with ID " + ecKey.getKeyID());
				}
			}

			OctetSequenceKey secretKey = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.generateKey(keyIDs.addRandomUniqueKeyID());
			keys.add(secretKey);
			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new symmetric encryption " + JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[0] + " bit key with ID " + secretKey.getKeyID());
			}

			return keys;
		}


		/**
		 * Generates a new set of permanent keys.
		 *
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The generated keys.
		 */
		public static List<JWK> generatePermanentKeys(final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			List<JWK> keys = new LinkedList<>();

			OctetSequenceKey hmacKey = JWKsSpec.OP.HMAC.generateKey();
			keys.add(hmacKey);
			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new HMAC SHA " + JWKsSpec.OP.HMAC.KEY_BIT_SIZE + " bit key with ID " + hmacKey.getKeyID());
			}

			OctetSequenceKey subjectKey = JWKsSpec.OP.SubjectEncryption.generateKey();
			keys.add(subjectKey);
			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new subject encryption AES SIV " + JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0] + " bit key with ID " + subjectKey.getKeyID());
			}

			OctetSequenceKey refreshTokenKey = JWKsSpec.OP.RefreshTokenEncryption.generateKey();
			keys.add(refreshTokenKey);
			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new refresh token encryption AES SIV " + JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE + " bit key with ID " + refreshTokenKey.getKeyID());
			}

			return keys;
		}


		/**
		 * Generates the missing permanent keys not found in the
		 * specified JWK set.
		 *
		 * @param jwkSet           The JWK set.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The generated missing permanent keys, empty list if
		 *         none.
		 */
		public static List<JWK> generateMissingPermanentKeys(final JWKSet jwkSet,
								     final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			List<JWK> keys = new LinkedList<>();

			if (new JWKSelector(JWKsSpec.OP.HMAC.KEY_MATCHER).select(jwkSet).isEmpty()) {
				OctetSequenceKey hmacKey = JWKsSpec.OP.HMAC.generateKey();
				keys.add(hmacKey);
				if (eventMessageSink != null) {
					eventMessageSink.accept("Generated missing HMAC SHA " + JWKsSpec.OP.HMAC.KEY_BIT_SIZE + " bit key with ID " + hmacKey.getKeyID());
				}
			}

			if (new JWKSelector(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER).select(jwkSet).isEmpty()) {
				OctetSequenceKey subjectKey = JWKsSpec.OP.SubjectEncryption.generateKey();
				keys.add(subjectKey);
				if (eventMessageSink != null) {
					eventMessageSink.accept("Generated missing subject encryption AES SIV " + JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0] + " bit key with ID " + subjectKey.getKeyID());
				}
			}

			if (new JWKSelector(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER).select(jwkSet).isEmpty()) {
				OctetSequenceKey refreshTokenKey = JWKsSpec.OP.RefreshTokenEncryption.generateKey();
				keys.add(refreshTokenKey);
				if (eventMessageSink != null) {
					eventMessageSink.accept("Generated missing refresh token encryption AES SIV " + JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE + " bit key with ID " + refreshTokenKey.getKeyID());
				}
			}

			return keys;
		}


		/**
		 * Generates a new JWK set.
		 *
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The JWK set.
		 */
		public static JWKSet generate(final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			List<JWK> keys = new LinkedList<>();
			keys.addAll(generateRotatingKeys(new KeyIDs(), eventMessageSink));
			keys.addAll(generatePermanentKeys(eventMessageSink));
			return new JWKSet(keys);
		}


		/**
		 * Generates a new JWK set.
		 *
		 * @param rsaKeyBitSize    The RSA key bit size. Must be one of
		 *                         {@link JWKsSpec#SUPPORTED_RSA_KEY_BIT_SIZES}.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The JWK set.
		 */
		public static JWKSet generate(final int rsaKeyBitSize,
					      final boolean noEdDSA,
					      final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			List<JWK> keys = new LinkedList<>();
			keys.addAll(generateRotatingKeys(new KeyIDs(), rsaKeyBitSize, noEdDSA, eventMessageSink));
			keys.addAll(generatePermanentKeys(eventMessageSink));
			return new JWKSet(keys);
		}


		/**
		 * A generates a new set of rotating signing and encryption
		 * keys and prefixes them to the specified JWK set.
		 *
		 * @param oldJWKSet        The Connect2id server JWK set.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The updated JWK set.
		 */
		public static JWKSet generateAndPrefixNewRotatingKeys(final JWKSet oldJWKSet,
								      final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			return generateAndPrefixNewRotatingKeys(oldJWKSet, JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE, false, eventMessageSink);
		}


		/**
		 * A generates a new set of rotating signing and encryption
		 * keys and prefixes them to the specified JWK set.
		 *
		 * @param oldJWKSet        The Connect2id server JWK set.
		 * @param rsaKeyBitSize    The RSA key bit size. Must be one of
		 *                         {@link JWKsSpec#SUPPORTED_RSA_KEY_BIT_SIZES}.
		 * @param noEdDSA          {@code true} to skip the EdDSA key
		 *                         generation.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The updated JWK set.
		 */
		public static JWKSet generateAndPrefixNewRotatingKeys(final JWKSet oldJWKSet,
								      final int rsaKeyBitSize,
								      final boolean noEdDSA,
								      final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			// Prefix so Connect2id server can roll over to new keys
			List<JWK> newGeneratedKeys = generateRotatingKeys(new KeyIDs(oldJWKSet), rsaKeyBitSize, noEdDSA, eventMessageSink);

			// Mark old matching keys as revoked
			var keyRevocation = new KeyRevocation(DateUtils.nowWithSecondsPrecision(), KeyRevocation.Reason.SUPERSEDED);

			JWKSet workJWKSet = oldJWKSet;
			for (JWK jwk : newGeneratedKeys) {
				workJWKSet = JWKUtils.revokeMatchingJWKs(
					workJWKSet,
					JWKUtils.createPurposeMatcher(jwk),
					keyRevocation);
			}

			JWKSet jwkSet = JWKUtils.prefixKeys(workJWKSet, newGeneratedKeys);

			if (eventMessageSink != null) {
				eventMessageSink.accept("Prefixed newly generated keys to existing JWK set");
			}

			return jwkSet;
		}


		public OP() {}
	}


	/**
	 * OpenID Federation 1.0 entity JWKs generator.
	 */
	public static class Federation {


		/**
		 * Generates a new set of rotating signing keys for an OpenID
		 * Federation 1.0 entity.
		 *
		 * @param reservedKeyIDs   The reserved key IDs, empty if none.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The generated rotating keys.
		 */
		public static List<JWK> generateRotatingKeys(final KeyIDs reservedKeyIDs,
							     final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			return generateRotatingKeys(reservedKeyIDs, JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE, eventMessageSink);
		}


		/**
		 * Generates a new set of rotating signing keys for an OpenID
		 * Federation 1.0 entity.
		 *
		 * @param reservedKeyIDs   The reserved key IDs, empty if none.
		 * @param rsaKeyBitSize    The RSA key bit size. Must be one of
		 *                         {@link JWKsSpec#SUPPORTED_RSA_KEY_BIT_SIZES}.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The generated rotating keys.
		 */
		public static List<JWK> generateRotatingKeys(final KeyIDs reservedKeyIDs,
						             final int rsaKeyBitSize,
						             final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			List<JWK> keys = new LinkedList<>();

			var keyIDs = new KeyIDs();
			keyIDs.addAll(reservedKeyIDs);

			RSAKey rsaKey = JWKsSpec.Federation.RotatedRSASigning.generateKey(keyIDs.addRandomUniqueKeyID(), rsaKeyBitSize);
			keys.add(rsaKey);
			if (eventMessageSink != null) {
				eventMessageSink.accept("Generated new signing RSA " + rsaKeyBitSize + " bit key with ID " + rsaKey.getKeyID());
			}

			return keys;
		}


		/**
		 * Generates a new JWK set for an OpenID Federation 1.0 entity.
		 *
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The JWK set.
		 */
		public static JWKSet generate(final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			return generate(JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE, eventMessageSink);
		}


		/**
		 * Generates a new JWK set for an OpenID Federation 1.0 entity.
		 *
		 * @param rsaKeyBitSize    The RSA key bit size. Must be one of
		 *                         {@link JWKsSpec#SUPPORTED_RSA_KEY_BIT_SIZES}.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The JWK set.
		 */
		public static JWKSet generate(final int rsaKeyBitSize,
					      final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			return new JWKSet(generateRotatingKeys(new KeyIDs(), rsaKeyBitSize, eventMessageSink));
		}


		/**
		 * Generates a new set of rotating signing keys and prefixes
		 * them to the specified OpenID Federation 1.0 entity JWK set.
		 *
		 * @param oldJWKSet        The OpenID Federation 1.0 entity JWK
		 *                         set.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The updated JWK set.
		 */
		public static JWKSet generateAndPrefixNewRotatingKeys(final JWKSet oldJWKSet,
								      final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			return generateAndPrefixNewRotatingKeys(oldJWKSet, JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE, eventMessageSink);
		}


		/**
		 * Generates a new set of rotating signing keys and prefixes
		 * them to the specified OpenID Federation 1.0 entity JWK set.
		 *
		 * @param oldJWKSet        The OpenID Federation 1.0 entity JWK
		 *                         set.
		 * @param rsaKeyBitSize    The RSA key bit size. Must be one of
		 *                         {@link JWKsSpec#SUPPORTED_RSA_KEY_BIT_SIZES}.
		 * @param eventMessageSink Optional sink for event messages,
		 *                         {@code null} if not specified.
		 *
		 * @return The updated JWK set.
		 */
		public static JWKSet generateAndPrefixNewRotatingKeys(final JWKSet oldJWKSet,
								      final int rsaKeyBitSize,
								      final @Nullable Consumer<String> eventMessageSink)
			throws JOSEException {

			// Prefix so Connect2id server can roll over to new keys
			List<JWK> newGeneratedKeys = generateRotatingKeys(new KeyIDs(oldJWKSet), rsaKeyBitSize, eventMessageSink);

			// Mark old matching keys as revoked
			var keyRevocation = new KeyRevocation(DateUtils.nowWithSecondsPrecision(), KeyRevocation.Reason.SUPERSEDED);

			JWKSet workJWKSet = oldJWKSet;
			for (JWK jwk : newGeneratedKeys) {
				workJWKSet = JWKUtils.revokeMatchingJWKs(
					workJWKSet,
					JWKUtils.createPurposeMatcher(jwk),
					keyRevocation);
			}

			JWKSet jwkSet = JWKUtils.prefixKeys(workJWKSet, newGeneratedKeys);

			if (eventMessageSink != null) {
				eventMessageSink.accept("Prefixed newly generated keys to existing federation entity JWK set");
			}

			return jwkSet;
		}


		private Federation() {}
	}


	private JWKsGenerator() {}
}
