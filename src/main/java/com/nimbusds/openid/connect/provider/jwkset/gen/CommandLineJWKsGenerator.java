package com.nimbusds.openid.connect.provider.jwkset.gen;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;
import com.thetransactioncompany.json.pretty.PrettyJson;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;


/**
 * Command line generator for Connect2id server JWKs.
 */
public class CommandLineJWKsGenerator {


        private CommandLineJWKsGenerator() {}


        static final Set<String> CONTEXTS = new LinkedHashSet<>(List.of("key-store", "op", "federation"));


        /**
         * Prints the usage to standard output.
         */
        static void printUsage() {
                System.out.println("Usage:");
                System.out.println("");
                System.out.println("java -jar jwks_gen.jar {key-store|op|federation} [options] [IN_FILE] OUT_FILE");
                System.out.println("");
                System.out.println("Examples:");
                System.out.println("1) Generate a new key store encryption JWK: ");
                System.out.println("   java -jar jwks-gen.jar key-store jwk.json");
                System.out.println("2) BASE64URL encode the output JWK: ");
                System.out.println("   java -jar jwks-gen.jar key-store -b64 jwk.json.b64");
                System.out.println("3) Generate a new OpenID provider JWK set: ");
                System.out.println("   java -jar jwks-gen.jar op jwkSet.json");
                System.out.println("4) Generate a new set of rotating keys and add to an existing OpenID provider\n" +
                                   "   JWK set; if a required permanent key is found missing it will also be\n" +
                                   "   generated and included in the JWK set (when upgrading to a newer Connect2id\n" +
                                   "   server release that requires a new type of permanent key):");
                System.out.println("   java -jar jwks-gen.jar op oldJWKSet.json newJWKSet.json");
                System.out.println("5) BASE64URL encode the output JWK set: ");
                System.out.println("   java -jar jwks-gen.jar op -b64 oldJWKSet.json newJWKSet.json.b64");
                System.out.println("6) Specify an RSA key size other than the default 2048 bits: ");
                System.out.println("   java -jar jwks-gen.jar op -rsa 4096 jwkSet.json");
                System.out.println("7) Do not generate an EdDSA signing key: ");
                System.out.println("   java -jar jwks-gen.jar op -noEdDSA jwkSet.json");
                System.out.println("8) Generate a new OpenID Federation 1.0 entity JWK set: ");
                System.out.println("   java -jar jwks-gen.jar federation federationJWKSet.json");
                System.out.println("9) Generate a new set of rotating keys and add to an existing OpenID Federation\n" +
                                   "   1.0 entity JWK set: ");
                System.out.println("   java -jar jwks-gen.jar federation oldFedJWKSet.json newFedJWKSet.json");
        }


        /**
         * Console method for generating a new Connect2id server JWK set, or
         * updating an existing JWK set with new signing and encryption keys.
         *
         * @param args The command line arguments.
         */
        public static void main(final String[] args) {
                
                String softwareVersion = JWKsGenerator.class.getPackage().getImplementationVersion();
                
                System.out.println("JWKs generator " + (softwareVersion != null ? "v" + softwareVersion + " " : "") + "for Connect2id server v6.x+\n");
                
                // create Options object
                var options = new Options();
                options.addOption("rsa", true, "RSA key size");
                options.addOption("noEdDSA", false, "No EdDSA key");
                options.addOption("b64", false, "BASE64URL encode the output JWK set");

                CommandLineParser parser = new DefaultParser();
                CommandLine commandLine;
                try {
                        commandLine = parser.parse( options, args);
                } catch (org.apache.commons.cli.ParseException e) {
                        System.err.println( "Command line parse error: " + e.getMessage());
                        printUsage();
                        return;
                }

                int rsaKeyBitSize = JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE;
                if(commandLine.hasOption("rsa")) {
                        try {
                                rsaKeyBitSize = Integer.parseInt(commandLine.getOptionValue("rsa"));
                        } catch (NumberFormatException e) {
                                System.err.println("RSA key bit size must be one of " + JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES);
                                printUsage();
                                return;
                        }
                }

                if (! JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES.contains(rsaKeyBitSize)) {
                        System.err.println("RSA key bit size must be one of " + JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES);
                        printUsage();
                        return;
                }

                final boolean noEdDSA = commandLine.hasOption("noEdDSA");

                final boolean b64Encode = commandLine.hasOption("b64");

                final List<String> argList = commandLine.getArgList();

                final String context;
                final File oldJWKSetFile;
                final File newJWKSetFile;

                if (argList.size() == 3) {
                        context = argList.get(0);
                        oldJWKSetFile = new File(argList.get(1));
                        newJWKSetFile = new File(argList.get(2));
                } else if (argList.size() == 2) {
                        context = argList.get(0);
                        newJWKSetFile = new File(argList.get(1));
                        oldJWKSetFile = null;
                } else {
                        System.err.println("Missing required arguments");
                        printUsage();
                        return;
                }

                if (! CONTEXTS.contains(context)) {
                        System.err.println("Unknown context: " + context);
                        printUsage();
                        return;
                }

                if ("key-store".equals(context) && oldJWKSetFile != null) {
                        System.out.println("IN_FILE not compatible with key-store context");
                        printUsage();
                        return;
                }
                
                JWKSet oldJWKSet = null;
                
                if (oldJWKSetFile != null) {
                        try {
                                oldJWKSet = JWKSet.load(oldJWKSetFile);
                        } catch (IOException | ParseException e) {
                                System.err.println("Couldn't read old JWK set file: " + e.getMessage());
                                return;
                        }
                }
                
                final Consumer<String> eventMessageSink = new Consumer<>() {

                        private final NumberedEventPrinter printer = new NumberedEventPrinter();

                        @Override
                        public void accept(final String eventMessage) {
                                printer.print(eventMessage);
                        }
                };


                String json;
                try {
                        if ("key-store".equals(context)) {

                                json = JWKsGenerator.KeyStore.generate(eventMessageSink)
                                        .toString();

                        } else if ("op".equals(context)) {

                                JWKSet newJWKSet;
                                if (oldJWKSet == null) {
                                        newJWKSet = JWKsGenerator.OP.generate(rsaKeyBitSize, noEdDSA, eventMessageSink);
                                } else {
                                        newJWKSet = JWKsGenerator.OP.generateAndPrefixNewRotatingKeys(oldJWKSet, rsaKeyBitSize, noEdDSA, eventMessageSink);
                                }
                                // Check if any of the required permanent keys are missing after an upgrade to a newer
                                // Connect2id server and generate them
                                List<JWK> missingPermanentKeys = JWKsGenerator.OP.generateMissingPermanentKeys(newJWKSet, eventMessageSink);
                                if (! missingPermanentKeys.isEmpty()) {
                                        newJWKSet = JWKUtils.postfixKeys(newJWKSet, missingPermanentKeys);
                                        eventMessageSink.accept("Added generated permanent keys to existing JWK set");
                                }

                                json = newJWKSet.toString(false);

                        } else if ("federation".equals(context)) {

                                if (oldJWKSet == null) {
                                        json = JWKsGenerator.Federation.generate(rsaKeyBitSize, eventMessageSink)
                                                .toString(false);
                                } else {
                                        json = JWKsGenerator.Federation.generateAndPrefixNewRotatingKeys(oldJWKSet, rsaKeyBitSize, eventMessageSink)
                                                .toString(false);
                                }

                        } else {
                                throw new IllegalStateException();
                        }
                } catch (JOSEException e) {
                        System.err.println("Couldn't generate JWK: " + e.getMessage());
                        return;
                }
                
                String output;
                if (b64Encode) {
                        output = Base64URL.encode(json).toString();
                } else {
                        try {
                                output = new PrettyJson(PrettyJson.Style.COMPACT).parseAndFormat(json);
                        } catch (ParseException e) {
                                System.err.println("Couldn't format JSON: " + e.getMessage());
                                return;
                        }
                }
                
                try {
                        var writer = new PrintWriter(newJWKSetFile, StandardCharsets.UTF_8);
                        writer.write(output);
                        writer.write("\n");
                        writer.close();
                } catch (IOException e) {
                        System.err.println("Couldn't write new JWK set file: " + e.getMessage());
                }
        }
}
