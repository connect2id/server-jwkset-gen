package com.nimbusds.openid.connect.provider.jwkset.validator;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;


public class JWKsValidatorTest {
	
	
	// OP

	@Test
	public void opPass() throws JOSEException {
		
		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.OP.RotatedRSASigning.generateKey("1"),
				JWKsSpec.OP.HMAC.generateKey(),
				JWKsSpec.OP.SubjectEncryption.generateKey(),
				JWKsSpec.OP.RefreshTokenEncryption.generateKey()
			)
		);
		
		JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);
	}


	@Test
	public void opAllowSetWithPublicKey() throws JOSEException {

		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.OP.RotatedRSASigning.generateKey("1"),
				JWKsSpec.OP.HMAC.generateKey(),
				JWKsSpec.OP.SubjectEncryption.generateKey(),
				JWKsSpec.OP.RefreshTokenEncryption.generateKey(),
				JWKsSpec.OP.RotatedRSASigning.generateKey("historic-key").toPublicJWK()
			)
		);
		
		JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);

		assertFalse(jwkSet.getKeyByKeyId("historic-key").isPrivate());
	}
	

	@Test
	public void opMissingSigningRSAKey() throws JOSEException {
		
		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.OP.HMAC.generateKey(),
				JWKsSpec.OP.SubjectEncryption.generateKey(),
				JWKsSpec.OP.RefreshTokenEncryption.generateKey()
			)
		);
		
		try {
			JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals("The JWK set must contain at least one signing RSA key pair with properties: kty=RSA use=sig alg=[RS256, null] with_id_only=true private_only=true non_revoked_only=true size=[2048, 3072, 4096]", e.getMessage());
		}
	}
	

	@Test
	public void opMissingHMACKey() throws JOSEException {
		
		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.OP.RotatedRSASigning.generateKey("1"),
				JWKsSpec.OP.SubjectEncryption.generateKey(),
				JWKsSpec.OP.RefreshTokenEncryption.generateKey()
			)
		);
		
		try {
			JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals("The JWK set must contain one HMAC secret key with properties: kty=oct use=sig kid=hmac private_only=true non_revoked_only=true size=256", e.getMessage());
		}
	}
	

	@Test
	public void opMissingSubjectEncryptKey() throws JOSEException {
		
		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.OP.RotatedRSASigning.generateKey("1"),
				JWKsSpec.OP.HMAC.generateKey(),
				JWKsSpec.OP.RefreshTokenEncryption.generateKey()
			)
		);
		
		try {
			JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals("The JWK set must contain one subject encryption AES key with properties: kty=oct use=enc kid=subject-encrypt private_only=true non_revoked_only=true size=[256, 384, 512]", e.getMessage());
		}
	}
	

	@Test
	public void opMissingRefreshTokenEncryptKey() throws JOSEException {
		
		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.OP.RotatedRSASigning.generateKey("1"),
				JWKsSpec.OP.HMAC.generateKey(),
				JWKsSpec.OP.SubjectEncryption.generateKey()
			)
		);
		
		try {
			JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals("The JWK set must contain one refresh token encryption AES key with properties: kty=oct use=enc kid=refresh-token-encrypt private_only=true non_revoked_only=true size=256", e.getMessage());
		}
	}


	// Federation

	@Test
	public void federationPass() throws JOSEException {

		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.Federation.RotatedRSASigning.generateKey("1")
			)
		);

		JWKsValidator.Federation.validatePresenceOfMinimumRequiredKeys(jwkSet);
	}


	@Test
	public void federationAllowSetWithPublicKey() throws JOSEException {

		var jwkSet = new JWKSet(
			List.of(
				JWKsSpec.Federation.RotatedRSASigning.generateKey("1"),
				JWKsSpec.Federation.RotatedRSASigning.generateKey("historic-key").toPublicJWK()
			)
		);

		JWKsValidator.Federation.validatePresenceOfMinimumRequiredKeys(jwkSet);

		assertFalse(jwkSet.getKeyByKeyId("historic-key").isPrivate());
	}


	@Test
	public void testMissingSigningRSAKey() {

		var jwkSet = new JWKSet();

		try {
			JWKsValidator.Federation.validatePresenceOfMinimumRequiredKeys(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals("The JWK set must contain at least one signing RSA key pair with properties: kty=RSA use=sig alg=[RS256, null] with_id_only=true private_only=true size=[2048, 3072, 4096]", e.getMessage());
		}
	}
}
