package com.nimbusds.openid.connect.provider.jwkset.validator;


import junit.framework.TestCase;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.crypto.bc.BouncyCastleProviderSingleton;
import com.nimbusds.jose.jwk.Curve;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;


public class SigningJWKValidatorTest extends TestCase {

	
	public void testValidateGoodRSAJWK()
		throws Exception {

		SigningJWKValidator.testSignAndVerify(JWKsSpec.OP.RotatedRSASigning.generateKey("1"), null, null);
	}

	
	public void testValidateGoodRSAJWK_deprecated()
		throws Exception {

		SigningJWKValidator.testSignAndVerify(JWKsSpec.OP.RotatedRSASigning.generateKey("1"), null, null);
	}

	
	public void testValidateBadRSAJWK_privateKeyMissing() {

		try {
			SigningJWKValidator.testSignAndVerify(JWKsSpec.OP.RotatedRSASigning.generateKey("1").toPublicJWK(), null, null);
			fail();
		} catch (JOSEException e) {
			assertEquals("The RSA JWK (kid=1) has no private part", e.getMessage());
		}
	}

	
	public void testValidateGoodECJWK()
		throws Exception {
		
		for (Curve crv: JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES) {
			
			SigningJWKValidator.testSignAndVerify(
				JWKsSpec.OP.RotatedECSigning.generateKey(crv, "1"),
				BouncyCastleProviderSingleton.getInstance(),
				BouncyCastleProviderSingleton.getInstance()
			);
		}
	}

	
	public void testValidateGoodECJWK_deprecated()
		throws Exception {
		
		for (Curve crv: JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES) {
			
			SigningJWKValidator.testSignAndVerify(
				JWKsSpec.OP.RotatedECSigning.generateKey(crv, "1"),
				BouncyCastleProviderSingleton.getInstance(),
				BouncyCastleProviderSingleton.getInstance()
			);
		}
	}

	
	public void testValidateBadECJWK_privateKeyMissing() {
		
		for (Curve crv: JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES) {
			
			try {
				SigningJWKValidator.testSignAndVerify(
					JWKsSpec.OP.RotatedECSigning.generateKey(crv, "1").toPublicJWK(),
					BouncyCastleProviderSingleton.getInstance(),
					BouncyCastleProviderSingleton.getInstance()
				);
				fail();
			} catch (JOSEException e) {
				assertEquals("The EC JWK (crv=" + crv + " kid=1) has no private part", e.getMessage());
			}
		}
	}

	
	public void testValidateGoodEdDSA25519JWK()
		throws Exception {

		SigningJWKValidator.testSignAndVerify(JWKsSpec.OP.RotatedEdDSASigning.generateKey("1"));
	}

	
	public void testValidateBadEdDSA25519JWK_privateKeyMissing() {

		try {
			SigningJWKValidator.testSignAndVerify(JWKsSpec.OP.RotatedEdDSASigning.generateKey("1").toPublicJWK());
			fail();
		} catch (JOSEException e) {
			assertEquals("The OKP JWK (kid=1) has not private part", e.getMessage());
		}
	}

	
	public void testValidateExampleRSAKey()
		throws Exception {

		String json = "{\"kty\":\"RSA\"," +
			"\"use\":\"sig\"," +
			"\"kid\":\"CXup\"," +
			"\"n\":\"hrwD-lc-IwzwidCANmy4qsiZk11yp9kHykOuP0yOnwi36VomYTQVEzZXgh2sDJpGgAutdQudgwLoV8tVSsTG9SQHgJjH9Pd_9V4Ab6PANyZNG6DSeiq1QfiFlEP6Obt0JbRB3W7X2vkxOVaNoWrYskZodxU2V0ogeVL_LkcCGAyNu2jdx3j0DjJatNVk7ystNxb9RfHhJGgpiIkO5S3QiSIVhbBKaJHcZHPF1vq9g0JMGuUCI-OTSVg6XBkTLEGw1C_R73WD_oVEBfdXbXnLukoLHBS11p3OxU7f4rfxA_f_72_UwmWGJnsqS3iahbms3FkvqoL9x_Vj3GhuJSf97Q\"," +
			"\"e\":\"AQAB\"," +
			"\"d\":\"bmpuqB4PIhJcndRs_i0jOXKjyQzwBXXq2GuWxPEsgFBYx7fFdCuGifQiytMeSEW2OQFY6W7XaqJbXneYMmoI0qTwMQcD91FNX_vlR5he0dNlpZqqYsvVN3c_oT4ENoPUr4GF6L4Jz74gBOlVsE8rvw3MVqrfmbF543ONBJPUt3d1TjKwaZQlgPji-ycGg_P7K-dKxpyfQsC8xMmVmiAF4QQtnUa9vMgiChiO8-6VzGm2yWWyIUVRLxSohrbSNFhqF2zeWXePAw0_nzeZh3IDIMS5ABo92Pry4N3X-X7v_7nf8MGngK4duQ_1UkkLk-3u0I3tk_glsarDN0tYhzPwAQ\"}";

		RSAKey rsaJWK = RSAKey.parse(json);
		
		SigningJWKValidator.testSignAndVerify(rsaJWK, null, null);
	}
}
