package com.nimbusds.openid.connect.provider.jwkset;


import com.nimbusds.jose.EncryptionMethod;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWEAlgorithm;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.util.ByteUtils;
import org.junit.Test;

import javax.crypto.SecretKey;
import java.security.SecureRandom;
import java.util.*;

import static org.junit.Assert.*;


public class JWKsSpecTest {
	
	
	@Test
	public void keySizeConstants() {
		
		// RSA signing and encryption keys
		assertTrue(JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES.contains(2048));
		assertTrue(JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES.contains(3072));
		assertTrue(JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES.contains(4096));
		assertEquals(3, JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES.size());
		
		// Rotated keys
		assertEquals(2048, JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE);
		
		assertEquals(128, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[0]);
		assertEquals(192, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[1]);
		assertEquals(256, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[2]);
		assertEquals(384, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[3]);
		assertEquals(512, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[4]);
		assertEquals(5, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES.length);
		
		// Permanent keys
		assertEquals(256, JWKsSpec.OP.HMAC.KEY_BIT_SIZE);
		
		assertEquals(256, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0]);
		assertEquals(384, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[1]);
		assertEquals(512, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[2]);
		assertEquals(3, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES.length);
		
		assertEquals(256, JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE);
	}
	
	
	@Test
	public void rotatedRSASigning_keyMatcher() {
		
		// JWS RSA alg specified
		JWKMatcher m = JWKsSpec.OP.RotatedRSASigning.createKeyMatcher(JWSAlgorithm.RS256);
		
		assertEquals(Collections.singleton(KeyType.RSA), m.getKeyTypes());
		assertTrue(m.isWithKeyIDOnly());
		assertEquals(Collections.singleton(KeyUse.SIGNATURE), m.getKeyUses());
		assertEquals(new HashSet<>(Arrays.asList(JWSAlgorithm.RS256, null)), m.getAlgorithms());
		assertTrue(m.isPrivateOnly());
		assertFalse(m.isPublicOnly());
		assertEquals(Set.of(2048, 3072, 4096), m.getKeySizes());
		assertEquals(0, m.getMinKeySize());
		assertEquals(0, m.getMaxKeySize());
		assertNull(m.getKeyOperations());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());

		// JWS RSA alg NOT specified
		m = JWKsSpec.OP.RotatedRSASigning.createKeyMatcher(null);
		
		assertEquals(Collections.singleton(KeyType.RSA), m.getKeyTypes());
		assertTrue(m.isWithKeyIDOnly());
		assertEquals(Collections.singleton(KeyUse.SIGNATURE), m.getKeyUses());
		assertEquals(Collections.singleton(null), m.getAlgorithms());
		assertTrue(m.isPrivateOnly());
		assertFalse(m.isPublicOnly());
		assertEquals(Set.of(2048, 3072, 4096), m.getKeySizes());
		assertEquals(0, m.getMinKeySize());
		assertEquals(0, m.getMaxKeySize());
		assertNull(m.getKeyOperations());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());
	}
	
	
	@Test
	public void rotatedRSASigning_generateKey() throws JOSEException {
		
		RSAKey jwk = JWKsSpec.OP.RotatedRSASigning.generateKey("123");
		assertTrue(JWKsSpec.OP.RotatedRSASigning.createKeyMatcher(null).matches(jwk));
		assertEquals("123", jwk.getKeyID());
	}
	
	
	@Test
	public void rotatedRSASigning_load_success() throws JOSEException {
		
		RSAKey jwk = JWKsSpec.OP.RotatedRSASigning.generateKey("123");
		var jwkSet = new JWKSet(jwk);
		
		for (JWSAlgorithm alg: JWSAlgorithm.Family.RSA) {
			List<RSAKey> keys = JWKsSpec.OP.RotatedRSASigning.loadKeys(jwkSet, alg);
			assertEquals(jwk, keys.get(0));
			assertEquals(1, keys.size());
		}
	}


	@Test
	public void rotatedRSASigning_load_nullAlg_success() throws JOSEException {

		RSAKey jwk = JWKsSpec.OP.RotatedRSASigning.generateKey("123");
		var jwkSet = new JWKSet(jwk);

		List<RSAKey> keys = JWKsSpec.OP.RotatedRSASigning.loadKeys(jwkSet, null);
		
		assertEquals(jwk, keys.get(0));
		assertEquals(1, keys.size());
	}
	
	
	@Test
	public void rotatedRSASigning_load_success_3072bit() throws JOSEException {
		
		RSAKey jwk = JWKsSpec.OP.RotatedRSASigning.generateKey("1", 3072);
		var jwkSet = new JWKSet(jwk);
		
		for (JWSAlgorithm alg: JWSAlgorithm.Family.RSA) {
			List<RSAKey> keys = JWKsSpec.OP.RotatedRSASigning.loadKeys(jwkSet, alg);
			assertEquals(jwk, keys.get(0));
			assertEquals(1, keys.size());
		}
	}
	
	
	@Test
	public void rotatedRSASigning_load_success_4096bit() throws JOSEException {
		
		RSAKey jwk = JWKsSpec.OP.RotatedRSASigning.generateKey("1", 4096);
		var jwkSet = new JWKSet(jwk);
		
		for (JWSAlgorithm alg: JWSAlgorithm.Family.RSA) {
			List<RSAKey> keys = JWKsSpec.OP.RotatedRSASigning.loadKeys(jwkSet, alg);
			assertEquals(jwk, keys.get(0));
			assertEquals(1, keys.size());
		}
	}
	
	
	@Test
	public void rotatedRSASigning_load_empty() throws JOSEException {
		
		var jwkSet = new JWKSet();
		
		for (JWSAlgorithm alg: JWSAlgorithm.Family.RSA) {
			List<RSAKey> keys = JWKsSpec.OP.RotatedRSASigning.loadKeys(jwkSet, alg);
			assertTrue(keys.isEmpty());
		}
	}
	
	
	@Test
	public void rotatedRSASigning_load_unsupportedAlgorithm() throws JOSEException {
		
		RSAKey jwk = JWKsSpec.OP.RotatedRSASigning.generateKey("123");
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.RotatedRSASigning.loadKeys(jwkSet, JWSAlgorithm.ES256);
			fail();
		} catch (JOSEException e) {
			assertEquals("Invalid / unsupported RSA signature algorithm: ES256", e.getMessage());
		}
	}
	
	
	@Test
	public void rotatedECSigning_supportedCurves() {
		
		assertTrue(JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES.contains(Curve.P_256));
		assertTrue(JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES.contains(Curve.P_384));
		assertTrue(JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES.contains(Curve.P_521));
		assertTrue(JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES.contains(Curve.SECP256K1));
		assertEquals(4, JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES.size());
	}
	
	
	@Test
	public void rotatedECSigning_keyMatcher() {
		
		for (JWSAlgorithm alg : JWSAlgorithm.Family.EC) {
			
			JWKMatcher m = JWKsSpec.OP.RotatedECSigning.createKeyMatcher(alg);
			assertEquals(m.getKeyTypes(), Collections.singleton(KeyType.EC));
			assertTrue(m.isPrivateOnly());
			assertEquals(m.getKeyUses(), Collections.singleton(KeyUse.SIGNATURE));
			assertEquals(m.getCurves(), Curve.forJWSAlgorithm(alg));
			assertEquals(new HashSet<>(Arrays.asList(alg, null)), m.getAlgorithms());
			assertTrue(m.isWithKeyIDOnly());
			assertNull(m.getKeyIDs());
			assertNull(m.getKeyOperations());
			assertEquals(0, m.getMinKeySize());
			assertEquals(0, m.getMaxKeySize());
			assertFalse(m.isNonRevokedOnly());
			assertFalse(m.isRevokedOnly());
		}
	}
	
	
	@Test
	public void rotatedECSigning_keyMatcher_nullJWSAlg() {
		
		try {
			JWKsSpec.OP.RotatedECSigning.createKeyMatcher(null);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid / unsupported EC DSA algorithm: null", e.getMessage());
		}
	}
	
	
	@Test
	public void rotatedECSigning_keyMatcher_unsupportedJWSAlg() {
		
		try {
			JWKsSpec.OP.RotatedECSigning.createKeyMatcher(JWSAlgorithm.RS256);
			fail();
		} catch (IllegalArgumentException e) {
			assertEquals("Invalid / unsupported EC DSA algorithm: RS256", e.getMessage());
		}
	}
	
	
	@Test
	public void rotatedECSigning_generateKey() throws JOSEException {
		
		Map<Curve,JWSAlgorithm> algMap = new HashMap<>();
		algMap.put(Curve.P_256, JWSAlgorithm.ES256);
		algMap.put(Curve.P_384, JWSAlgorithm.ES384);
		algMap.put(Curve.P_521, JWSAlgorithm.ES512);
		algMap.put(Curve.SECP256K1, JWSAlgorithm.ES256K);
		
		for (Curve crv: JWKsSpec.OP.RotatedECSigning.SUPPORTED_CURVES) {
			ECKey jwk = JWKsSpec.OP.RotatedECSigning.generateKey(crv, "123");
			assertTrue(JWKsSpec.OP.RotatedECSigning.createKeyMatcher(algMap.get(crv)).matches(jwk));
			assertEquals("123", jwk.getKeyID());
		}
	}
	
	
	@Test
	public void rotatedECSigning_load_success() throws JOSEException {
		
		for (JWSAlgorithm alg: JWSAlgorithm.Family.EC) {
			
			ECKey jwk = JWKsSpec.OP.RotatedECSigning.generateKey(Curve.forJWSAlgorithm(alg).iterator().next(), "123");
			var jwkSet = new JWKSet(jwk);
		
			List<ECKey> keys = JWKsSpec.OP.RotatedECSigning.loadKeys(jwkSet, alg);
			assertEquals(jwk, keys.get(0));
			assertEquals(1, keys.size());
		}
	}
	
	
	@Test
	public void rotatedECSigning_load_empty() throws JOSEException {
		
		var jwkSet = new JWKSet();
		
		for (JWSAlgorithm alg: JWSAlgorithm.Family.EC) {
			List<ECKey> keys = JWKsSpec.OP.RotatedECSigning.loadKeys(jwkSet, alg);
			assertTrue(keys.isEmpty());
		}
	}
	
	
	@Test
	public void rotatedECSigning_load_unsupportedAlgorithm() throws JOSEException {
		
		ECKey jwk = JWKsSpec.OP.RotatedECSigning.generateKey(Curve.P_256, "123");
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.RotatedECSigning.loadKeys(jwkSet, JWSAlgorithm.RS256);
			fail();
		} catch (JOSEException e) {
			assertEquals("Invalid / unsupported EC DSA algorithm: RS256", e.getMessage());
		}
	}
	
	
	@Test
	public void rotatedEdDSASigning_keyMatcher() {
		
		JWKMatcher m = JWKsSpec.OP.RotatedEdDSASigning.KEY_MATCHER;
		
		assertEquals(Collections.singleton(KeyType.OKP), m.getKeyTypes());
		assertEquals(Collections.singleton(Curve.Ed25519), m.getCurves());
		assertEquals(new HashSet<>(Arrays.asList(JWSAlgorithm.EdDSA, null)), m.getAlgorithms());
		assertEquals(Collections.singleton(KeyUse.SIGNATURE), m.getKeyUses());
		assertTrue(m.isPrivateOnly());
		assertFalse(m.isPublicOnly());
		assertEquals(0, m.getMinKeySize());
		assertEquals(0, m.getMaxKeySize());
		assertNull(m.getKeySizes());
		assertNull(m.getKeyOperations());
		assertTrue(m.isWithKeyIDOnly());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());
	}
	
	
	@Test
	public void rotatedEdDSASigning_generateKey() throws JOSEException {
		
		OctetKeyPair jwk = JWKsSpec.OP.RotatedEdDSASigning.generateKey("123");
		assertTrue(JWKsSpec.OP.RotatedEdDSASigning.KEY_MATCHER.matches(jwk));
		assertEquals("123", jwk.getKeyID());
	}
	
	
	@Test
	public void rotatedEdDSASigning_load_success() throws JOSEException {
		
		OctetKeyPair jwk = JWKsSpec.OP.RotatedEdDSASigning.generateKey("123");
		
		var jwkSet = new JWKSet(jwk);
		List<OctetKeyPair> keys = JWKsSpec.OP.RotatedEdDSASigning.loadKeys(jwkSet);
		assertEquals(jwk, keys.get(0));
		assertEquals(1, keys.size());
	}
	
	
	@Test
	public void rotatedEdDSASigning_load_empty() {
		
		var jwkSet = new JWKSet();
		List<OctetKeyPair> keys = JWKsSpec.OP.RotatedEdDSASigning.loadKeys(jwkSet);
		assertTrue(keys.isEmpty());
	}
	
	
	@Test
	public void rotatedRSAEncryption_keyMatcher() {
		
		JWKMatcher m = JWKsSpec.OP.RotatedRSAEncryption.KEY_MATCHER;
		
		assertEquals(Collections.singleton(KeyType.RSA), m.getKeyTypes());
		assertTrue(m.isPrivateOnly());
		assertEquals(Collections.singleton(KeyUse.ENCRYPTION), m.getKeyUses());
		assertEquals(Set.of(2048, 3072, 4096), m.getKeySizes());
		assertTrue(m.isWithKeyIDOnly());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());
	}
	
	
	@Test
	public void rotatedECDHEncryption_supportedCurves() {
		
		assertTrue(JWKsSpec.OP.RotatedECDHEncryption.SUPPORTED_CURVES.contains(Curve.P_256));
		assertTrue(JWKsSpec.OP.RotatedECDHEncryption.SUPPORTED_CURVES.contains(Curve.P_384));
		assertTrue(JWKsSpec.OP.RotatedECDHEncryption.SUPPORTED_CURVES.contains(Curve.P_521));
		assertEquals(3, JWKsSpec.OP.RotatedECDHEncryption.SUPPORTED_CURVES.size());
	}
	
	
	@Test
	public void rotatedECDHEncryption_keyMatcher() throws JOSEException {
		
		JWKMatcher m = JWKsSpec.OP.RotatedECDHEncryption.KEY_MATCHER;
		
		assertEquals(Collections.singleton(KeyType.EC), m.getKeyTypes());
		assertTrue(m.isPrivateOnly());
		assertEquals(Collections.singleton(KeyUse.ENCRYPTION), m.getKeyUses());
		assertTrue(m.isWithKeyIDOnly());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());
		
		assertTrue(m.matches(JWKsSpec.OP.RotatedECDHEncryption.generateKey(Curve.P_256, "1")));
		assertTrue(m.matches(JWKsSpec.OP.RotatedECDHEncryption.generateKey(Curve.P_384, "1")));
		assertTrue(m.matches(JWKsSpec.OP.RotatedECDHEncryption.generateKey(Curve.P_521, "1")));
		
		assertFalse(m.matches(JWKsSpec.OP.RotatedECDHEncryption.generateKey(Curve.P_256, null)));
		assertFalse(m.matches(JWKsSpec.OP.RotatedECDHEncryption.generateKey(Curve.P_384, null)));
		assertFalse(m.matches(JWKsSpec.OP.RotatedECDHEncryption.generateKey(Curve.P_521, null)));
	}
	
	
	@Test
	public void rotatedAccessTokenDirectEncryption_keySizes() {
		
		assertEquals(128, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[0]);
		assertEquals(192, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[1]);
		assertEquals(256, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[2]);
		assertEquals(384, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[3]);
		assertEquals(512, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES[4]);
		assertEquals(5, JWKsSpec.OP.RotatedAccessTokenDirectEncryption.KEY_BIT_SIZES.length);
	}
	
	
	@Test
	public void rotatedAccessTokenDirectEncryption_createKeyMatcher() {
		
		List<EncryptionMethod> encryptionMethods = new LinkedList<>();
		encryptionMethods.addAll(EncryptionMethod.Family.AES_GCM);
		encryptionMethods.addAll(EncryptionMethod.Family.AES_CBC_HMAC_SHA);
		encryptionMethods.add(EncryptionMethod.XC20P);
		
		for (EncryptionMethod enc: encryptionMethods) {
			
			JWKMatcher m = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.createKeyMatcher(enc);
			
			assertEquals(Collections.singleton(KeyType.OCT), m.getKeyTypes());
			assertTrue(m.isWithKeyIDOnly());
			assertEquals(Collections.singleton(KeyUse.ENCRYPTION), m.getKeyUses());
			assertEquals(new HashSet<>(Arrays.asList(JWEAlgorithm.DIR, null)), m.getAlgorithms());
			assertTrue(m.isPrivateOnly());
			assertEquals(0, m.getMinKeySize());
			assertEquals(0, m.getMaxKeySize());
			assertEquals(Collections.singleton(enc.cekBitLength()), m.getKeySizes());
			assertNull(m.getKeyOperations());
			assertFalse(m.isNonRevokedOnly());
			assertFalse(m.isRevokedOnly());
		}
	}
	
	
	@Test
	public void rotatedAccessTokenDirectEncryption_generateKey() throws JOSEException {
		
		assertEquals(128, EncryptionMethod.A128GCM.cekBitLength());
		
		OctetSequenceKey jwk = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.generateKey("123");
		assertTrue(JWKsSpec.OP.RotatedAccessTokenDirectEncryption.createKeyMatcher(EncryptionMethod.A128GCM).matches(jwk));
		assertEquals(128, jwk.size());
		assertEquals("123", jwk.getKeyID());
	}
	
	
	@Test
	public void rotatedAccessTokenDirectEncryption_generateKey_withCekSpec() throws JOSEException {
		
		List<EncryptionMethod> encryptionMethods = new LinkedList<>();
		encryptionMethods.addAll(EncryptionMethod.Family.AES_GCM);
		encryptionMethods.addAll(EncryptionMethod.Family.AES_CBC_HMAC_SHA);
		encryptionMethods.add(EncryptionMethod.XC20P);
		
		for (EncryptionMethod enc: encryptionMethods) {
			
			OctetSequenceKey jwk = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.generateKey("123", enc.cekBitLength());
			
			assertTrue(JWKsSpec.OP.RotatedAccessTokenDirectEncryption.createKeyMatcher(enc).matches(jwk));
			
			assertTrue(JWKsSpec.OP.RotatedAccessTokenDirectEncryption.createKeyMatcher(enc).matches(jwk));
			
			assertEquals(enc.cekBitLength(), jwk.size());
			assertEquals("123", jwk.getKeyID());
		}
	}
	
	
	@Test
	public void rotatedAccessTokenDirectEncryption_generateKey_bitSizeArg() throws JOSEException {

		Set<EncryptionMethod> encryptionMethods = new HashSet<>();
		encryptionMethods.addAll(EncryptionMethod.Family.AES_GCM);
		encryptionMethods.addAll(EncryptionMethod.Family.AES_CBC_HMAC_SHA);

		for (int bitSize: new int[]{128, 192, 256, 384, 512}) {
			
			OctetSequenceKey jwk = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.generateKey("123", bitSize);
			for (EncryptionMethod enc: encryptionMethods) {
				assertEquals(
					bitSize == enc.cekBitLength(),
					JWKsSpec.OP.RotatedAccessTokenDirectEncryption.createKeyMatcher(enc).matches(jwk)
				);
			}

			assertEquals(bitSize, jwk.size());
			assertEquals("123", jwk.getKeyID());
		}
	}
	
	
	@Test
	public void rotatedAccessTokenDirectEncryption_load_success() throws JOSEException {
		
		OctetSequenceKey jwk = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.generateKey("123");
		assertEquals(128, jwk.size());
		
		var jwkSet = new JWKSet(jwk);
		
		List<OctetSequenceKey> keys = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.loadKeys(jwkSet, EncryptionMethod.A128GCM);
		assertEquals(jwk, keys.get(0));
		assertEquals(1, keys.size());
		
		keys = JWKsSpec.OP.RotatedAccessTokenDirectEncryption.loadKeys(jwkSet, EncryptionMethod.A128GCM);
		assertEquals(jwk, keys.get(0));
		assertEquals(1, keys.size());
	}
	
	
	@Test
	public void rotatedAccessTokenDirectEncryption_load_empty() {
		
		var jwkSet = new JWKSet();
		
		assertTrue(JWKsSpec.OP.RotatedAccessTokenDirectEncryption.loadKeys(jwkSet, EncryptionMethod.A128GCM).isEmpty());
		
		assertTrue(JWKsSpec.OP.RotatedAccessTokenDirectEncryption.loadKeys(jwkSet, EncryptionMethod.A128GCM).isEmpty());
	}
	
	
	@Test
	public void hmac_keyID() {
		
		assertEquals("hmac", JWKsSpec.OP.HMAC.KEY_ID);
	}
	
	
	@Test
	public void hmac_keySize() {
		
		assertEquals(256, JWKsSpec.OP.HMAC.KEY_BIT_SIZE);
	}
	
	
	@Test
	public void hmac_keySelector() {
		
		JWKMatcher m = JWKsSpec.OP.HMAC.KEY_MATCHER;
		
		assertEquals(Collections.singleton(KeyType.OCT), m.getKeyTypes());
		assertEquals(Collections.singleton(256), m.getKeySizes());
		assertTrue(m.isPrivateOnly());
		assertFalse(m.isPublicOnly());
		assertEquals(Collections.singleton(KeyUse.SIGNATURE), m.getKeyUses());
		assertEquals(Collections.singleton("hmac"), m.getKeyIDs());
		assertNull(m.getAlgorithms());
		assertNull(m.getKeyOperations());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());
	}
	
	
	@Test
	public void hmac_generateKey() throws JOSEException {
		
		OctetSequenceKey jwk = JWKsSpec.OP.HMAC.generateKey();
		assertTrue(JWKsSpec.OP.HMAC.KEY_MATCHER.matches(jwk));
	}
	
	
	private static OctetSequenceKey generateOCT(final int bits, final KeyUse use, final String kid) {
		
		byte[] randomBits = new byte[ByteUtils.byteLength(bits)];
		new SecureRandom().nextBytes(randomBits);
		return new OctetSequenceKey.Builder(randomBits)
			.keyUse(use)
			.keyID(kid)
			.build();
	}
	
	
	@Test
	public void hmac_load_success() throws JOSEException {
		
		OctetSequenceKey oct = JWKsSpec.OP.HMAC.generateKey();
		
		SecretKey hmacKey = JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
		
		assertEquals("HmacSha256", hmacKey.getAlgorithm());
		assertArrayEquals(oct.getKeyValue().decode(), hmacKey.getEncoded());
	}
	
	
	@Test
	public void hmac_load_null() {
		
		try {
			JWKsSpec.OP.HMAC.loadKey(null);
			fail();
		} catch (JOSEException e) {
			assertEquals("Missing or empty JSON Web Key (JWK) set", e.getMessage());
		}
	}
	
	
	@Test
	public void hmac_load_empty() {
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet());
			fail();
		} catch (JOSEException e) {
			assertEquals("Missing or empty JSON Web Key (JWK) set", e.getMessage());
		}
	}
	
	
	private static final String BAD_HMAC_MESSAGE = "Couldn't find eligible secret JSON Web Key (JWK) for applying HMAC to objects: Required key ID \"hmac\", required key use \"sig\", required key size 256 bits";
	
	
	@Test
	public void hmac_load_invalidSize() {
		
		OctetSequenceKey oct = generateOCT(128, KeyUse.SIGNATURE, JWKsSpec.OP.HMAC.KEY_ID);
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_HMAC_MESSAGE, e.getMessage());
		}
		
		oct = generateOCT(JWKsSpec.OP.HMAC.KEY_BIT_SIZE - 1, KeyUse.SIGNATURE, JWKsSpec.OP.HMAC.KEY_ID);
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_HMAC_MESSAGE, e.getMessage());
		}
		
		oct = generateOCT(JWKsSpec.OP.HMAC.KEY_BIT_SIZE + 8, KeyUse.SIGNATURE, JWKsSpec.OP.HMAC.KEY_ID);
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_HMAC_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void hmac_load_nullKeyUse() {
		
		OctetSequenceKey oct = generateOCT(256, null, JWKsSpec.OP.HMAC.KEY_ID);
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_HMAC_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void hmac_load_otherKeyUse() {
		
		OctetSequenceKey oct = generateOCT(256, KeyUse.ENCRYPTION, JWKsSpec.OP.HMAC.KEY_ID);
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_HMAC_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void hmac_load_invalidKeyID() {
		
		OctetSequenceKey oct = generateOCT(256, KeyUse.ENCRYPTION, "HMAC");
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_HMAC_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void hmac_load_nullKeyID() {
		
		OctetSequenceKey oct = generateOCT(256, KeyUse.ENCRYPTION, null);
		
		try {
			JWKsSpec.OP.HMAC.loadKey(new JWKSet(oct));
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_HMAC_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void subjectEncryption_keyID() {
		
		assertEquals("subject-encrypt", JWKsSpec.OP.SubjectEncryption.KEY_ID);
	}
	
	
	@Test
	public void subjectEncryption_keySizes() {
		
		assertEquals(256, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0]);
		assertEquals(384, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[1]);
		assertEquals(512, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[2]);
		assertEquals(3, JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES.length);
	}
	
	
	@Test
	public void subjectEncryption_keyMatcher() {
		
		JWKMatcher m = JWKsSpec.OP.SubjectEncryption.KEY_MATCHER;
		
		assertEquals(Collections.singleton(KeyType.OCT), m.getKeyTypes());
		assertEquals(Set.of(256, 384, 512), m.getKeySizes());
		assertTrue(m.isPrivateOnly());
		assertFalse(m.isPublicOnly());
		assertEquals(Collections.singleton(KeyUse.ENCRYPTION), m.getKeyUses());
		assertEquals(Collections.singleton("subject-encrypt"), m.getKeyIDs());
		assertNull(m.getAlgorithms());
		assertNull(m.getKeyOperations());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());
	}
	
	
	@Test
	public void subjectEncryption_generateKey() throws JOSEException {
		
		OctetSequenceKey jwk = JWKsSpec.OP.SubjectEncryption.generateKey();
		assertTrue(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER.matches(jwk));
	}
	
	
	@Test
	public void subjectEncryption_load_success() throws JOSEException {
		
		OctetSequenceKey jwk = JWKsSpec.OP.SubjectEncryption.generateKey();
		
		var jwkSet = new JWKSet(jwk);
		
		SecretKey secretKey = JWKsSpec.OP.SubjectEncryption.loadKey(jwkSet);
		assertEquals("AES", secretKey.getAlgorithm());
		assertArrayEquals(jwk.toByteArray(), secretKey.getEncoded());
	}
	
	
	private static final String BAD_SUBJECT_ENCRYPTION_KEY_MESSAGE = "Couldn't find eligible secret JSON Web Key (JWK) for pairwise subject encryption: Required key ID \"subject-encrypt\", required key use \"enc\", required key sizes [256, 384, 512] bits";
	
	
	@Test
	public void subjectEncryption_load_invalidKeySize() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0] - 8, KeyUse.ENCRYPTION, JWKsSpec.OP.SubjectEncryption.KEY_ID);
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.SubjectEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_SUBJECT_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	@Test
	public void subjectEncryption_load_noKeyIDMatch() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0], KeyUse.ENCRYPTION, "1");
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.SubjectEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_SUBJECT_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void subjectEncryption_load_keyUseMissing() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0], null, JWKsSpec.OP.SubjectEncryption.KEY_ID);
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.SubjectEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_SUBJECT_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void subjectEncryption_load_keyUseOther() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.SubjectEncryption.KEY_BIT_SIZES[0], KeyUse.SIGNATURE, JWKsSpec.OP.SubjectEncryption.KEY_ID);
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.SubjectEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_SUBJECT_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void subjectEncryption_load_emptyJWKSet() {
		
		try {
			JWKsSpec.OP.SubjectEncryption.loadKey(new JWKSet());
			fail();
		} catch (JOSEException e) {
			assertEquals("Missing or empty JSON Web Key (JWK) set", e.getMessage());
		}
	}
	
	
	@Test
	public void subjectEncryption_load_tooMany() throws JOSEException {
		
		var jwkSet = new JWKSet(List.of(
			JWKsSpec.OP.SubjectEncryption.generateKey(),
			JWKsSpec.OP.SubjectEncryption.generateKey()));
		
		try {
			JWKsSpec.OP.SubjectEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals("Too many pairwise subject encryption keys, must be one", e.getMessage());
		}
	}
	
	
	@Test
	public void refreshTokenEncryption_keyID() {
		
		assertEquals("refresh-token-encrypt", JWKsSpec.OP.RefreshTokenEncryption.KEY_ID);
	}
	
	
	@Test
	public void refreshTokenEncryption_keySizes() {
		
		assertEquals(256, JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE);
	}
	
	
	@Test
	public void refreshTokenEncryption_keyMatcher() {
		
		JWKMatcher m = JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER;
		
		assertEquals(Collections.singleton(KeyType.OCT), m.getKeyTypes());
		assertEquals(Collections.singleton(256), m.getKeySizes());
		assertTrue(m.isPrivateOnly());
		assertFalse(m.isPublicOnly());
		assertEquals(Collections.singleton(KeyUse.ENCRYPTION), m.getKeyUses());
		assertEquals(Collections.singleton("refresh-token-encrypt"), m.getKeyIDs());
		assertNull(m.getAlgorithms());
		assertNull(m.getKeyOperations());
		assertFalse(m.isNonRevokedOnly());
		assertFalse(m.isRevokedOnly());
	}
	
	
	@Test
	public void refreshTokenEncryption_generateKey() throws JOSEException {
		
		OctetSequenceKey jwk = JWKsSpec.OP.RefreshTokenEncryption.generateKey();
		assertTrue(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER.matches(jwk));
	}
	
	
	@Test
	public void refreshTokenEncryption_load_success() throws JOSEException {
		
		OctetSequenceKey jwk = JWKsSpec.OP.RefreshTokenEncryption.generateKey();
		
		var jwkSet = new JWKSet(jwk);
		
		SecretKey secretKey = JWKsSpec.OP.RefreshTokenEncryption.loadKey(jwkSet);
		assertEquals("AES", secretKey.getAlgorithm());
		assertArrayEquals(jwk.toByteArray(), secretKey.getEncoded());
	}
	
	
	private static final String BAD_REFRESH_TOKEN_ENCRYPTION_KEY_MESSAGE = "Couldn't find eligible secret JSON Web Key (JWK) for refresh token encryption: Required key ID \"refresh-token-encrypt\", required key use \"enc\", required key size 256 bits";
	
	
	@Test
	public void refreshTokenEncryption_load_invalidKeySize() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE - 8, KeyUse.ENCRYPTION, JWKsSpec.OP.RefreshTokenEncryption.KEY_ID);
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.RefreshTokenEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_REFRESH_TOKEN_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	@Test
	public void refreshTokenEncryption_load_noKeyIDMatch() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE, KeyUse.ENCRYPTION, "1");
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.RefreshTokenEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_REFRESH_TOKEN_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void refreshTokenEncryption_load_keyUseMissing() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE, null, JWKsSpec.OP.RefreshTokenEncryption.KEY_ID);
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.RefreshTokenEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_REFRESH_TOKEN_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void refreshTokenEncryption_load_keyUseOther() {
		
		OctetSequenceKey jwk = generateOCT(JWKsSpec.OP.RefreshTokenEncryption.KEY_BIT_SIZE, KeyUse.SIGNATURE, JWKsSpec.OP.RefreshTokenEncryption.KEY_ID);
		
		var jwkSet = new JWKSet(jwk);
		
		try {
			JWKsSpec.OP.RefreshTokenEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals(BAD_REFRESH_TOKEN_ENCRYPTION_KEY_MESSAGE, e.getMessage());
		}
	}
	
	
	@Test
	public void refreshTokenEncryption_load_emptyJWKSet() {
		
		try {
			JWKsSpec.OP.RefreshTokenEncryption.loadKey(new JWKSet());
			fail();
		} catch (JOSEException e) {
			assertEquals("Missing or empty JSON Web Key (JWK) set", e.getMessage());
		}
	}
	
	
	@Test
	public void refreshTokenEncryption_load_tooMany() throws JOSEException {
		
		var jwkSet = new JWKSet(List.of(
			JWKsSpec.OP.RefreshTokenEncryption.generateKey(),
			JWKsSpec.OP.RefreshTokenEncryption.generateKey()));
		
		try {
			JWKsSpec.OP.RefreshTokenEncryption.loadKey(jwkSet);
			fail();
		} catch (JOSEException e) {
			assertEquals("Too many refresh token encryption keys, must be one", e.getMessage());
		}
	}
}
