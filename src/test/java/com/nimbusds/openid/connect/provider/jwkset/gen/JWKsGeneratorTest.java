package com.nimbusds.openid.connect.provider.jwkset.gen;


import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.jose.util.ByteUtils;
import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;
import com.nimbusds.openid.connect.provider.jwkset.validator.JWKsValidator;
import org.junit.Test;

import javax.crypto.SecretKey;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

import static org.junit.Assert.*;


public class JWKsGeneratorTest {


	private static boolean isAroundCurrentTime(final Date date) {
		return DateUtils.isWithin(date, DateUtils.nowWithSecondsPrecision(), 10);
	}
	

	@Test
	public void opGenerate()
		throws Exception {

		for (int rsaKeyBitSize: JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES) {

			final var eventMessageSink = new Consumer<String>() {
				private final NumberedEventPrinter printer = new NumberedEventPrinter();

				@Override
				public void accept(final String eventMessage) {
					printer.print(eventMessage);
				}
			};

			JWKSet jwkSet = rsaKeyBitSize == JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE ?
				JWKsGenerator.OP.generate(eventMessageSink) :
				JWKsGenerator.OP.generate(rsaKeyBitSize, false, eventMessageSink);

			JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);

			Iterator<JWK> jwkIterator = jwkSet.getKeys().iterator();

			JWK jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(rsaKeyBitSize, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_256, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_384, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_521, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.SECP256K1, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetKeyPair);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.Ed25519, ((OctetKeyPair) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(rsaKeyBitSize, jwk.size());
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(384, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(521, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertNotEquals("hmac", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(128, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("hmac", jwk.getKeyID());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.HMAC.KEY_MATCHER.matches(jwk));

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("subject-encrypt", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER.matches(jwk));

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("refresh-token-encrypt", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER.matches(jwk));
		}
	}
	

	@Test
	public void opGenerate_noEdDSA()
		throws Exception {

		for (int rsaKeyBitSize: JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES) {

			final var eventMessageSink = new Consumer<String>() {
				private final NumberedEventPrinter printer = new NumberedEventPrinter();

				@Override
				public void accept(final String eventMessage) {
					printer.print(eventMessage);
				}
			};

			JWKSet jwkSet = JWKsGenerator.OP.generate(rsaKeyBitSize, true, eventMessageSink);

			JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);

			Iterator<JWK> jwkIterator = jwkSet.getKeys().iterator();

			JWK jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(rsaKeyBitSize, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_256, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_384, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_521, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.SECP256K1, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(rsaKeyBitSize, jwk.size());
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(384, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(521, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertNotEquals("hmac", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(128, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("hmac", jwk.getKeyID());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.HMAC.KEY_MATCHER.matches(jwk));

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("subject-encrypt", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER.matches(jwk));

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("refresh-token-encrypt", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER.matches(jwk));
		}
	}


	@Test
	public void opAddNewKeys()
		throws Exception {

		for (int rsaKeyBitSize: JWKsSpec.SUPPORTED_RSA_KEY_BIT_SIZES) {

			final var eventMessageSink = new Consumer<String>() {
				private final NumberedEventPrinter printer = new NumberedEventPrinter();

				@Override
				public void accept(final String eventMessage) {
					printer.print(eventMessage);
				}
			};

			// Default RSA 2048 bit
			JWKSet oldJWKSet = JWKsGenerator.OP.generate(null);

			JWKSet newJWKSet = rsaKeyBitSize == JWKsSpec.DEFAULT_RSA_KEY_BIT_SIZE ?
				JWKsGenerator.OP.generateAndPrefixNewRotatingKeys(oldJWKSet, eventMessageSink) :
				JWKsGenerator.OP.generateAndPrefixNewRotatingKeys(oldJWKSet, rsaKeyBitSize, false, eventMessageSink);

			JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(newJWKSet);

			// Check that the new keys have been prepended
			var merged = new KeyIDs(newJWKSet);
			merged.addAll(new KeyIDs(newJWKSet));
			assertEquals(merged, new KeyIDs(newJWKSet));

			Iterator<JWK> jwkIterator = newJWKSet.getKeys().iterator();

			// New keys

			JWK jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(rsaKeyBitSize, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_256, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_384, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_521, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.SECP256K1, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetKeyPair);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.Ed25519, ((OctetKeyPair) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(rsaKeyBitSize, jwk.size());
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(384, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(521, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertNotEquals("hmac", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(128, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			// Revoked (superseded) keys
			jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(2048, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_256, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_384, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.P_521, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.SECP256K1, ((ECKey) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetKeyPair);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(Curve.Ed25519, ((OctetKeyPair) jwk).getCurve());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof RSAKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(2048, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(384, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof ECKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(521, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals(4, jwk.getKeyID().length());
			assertNotEquals("hmac", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(128, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
			assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

			// Permanent keys

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("hmac", jwk.getKeyID());
			assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.HMAC.KEY_MATCHER.matches(jwk));

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("subject-encrypt", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER.matches(jwk));

			jwk = jwkIterator.next();
			assertTrue(jwk instanceof OctetSequenceKey);
			assertTrue(jwk.isPrivate());
			assertEquals("refresh-token-encrypt", jwk.getKeyID());
			assertEquals(KeyUse.ENCRYPTION, jwk.getKeyUse());
			assertEquals(256, jwk.size());
			assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
			assertNull(jwk.getKeyRevocation());

			assertTrue(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER.matches(jwk));
		}
	}
	
	
	@Test
	public void opGenerateMissingPermanentKeys_setComplete()
		throws Exception {
		
		JWKSet completeJWKSet = new JWKSet(JWKsGenerator.OP.generatePermanentKeys(null));
		
		assertTrue(JWKsGenerator.OP.generateMissingPermanentKeys(completeJWKSet, null).isEmpty());
	}
	
	
	@Test
	public void opGenerateMissingPermanentKeys_missingHMACKey()
		throws Exception {
		
		var incompleteJWKSet = new JWKSet(Arrays.asList(
			JWKsSpec.OP.SubjectEncryption.generateKey(),
			JWKsSpec.OP.RefreshTokenEncryption.generateKey()
		));
		
		List<JWK> keysToAdd = JWKsGenerator.OP.generateMissingPermanentKeys(incompleteJWKSet, null);
		
		assertTrue(JWKsSpec.OP.HMAC.KEY_MATCHER.matches(keysToAdd.get(0)));
		
		assertEquals(1, keysToAdd.size());
	}
	
	
	@Test
	public void opGenerateMissingPermanentKeys_missingSubjectEncryptionKey()
		throws Exception {
		
		var incompleteJWKSet = new JWKSet(Arrays.asList(
			JWKsSpec.OP.HMAC.generateKey(),
			JWKsSpec.OP.RefreshTokenEncryption.generateKey()
		));
		
		List<JWK> keysToAdd = JWKsGenerator.OP.generateMissingPermanentKeys(incompleteJWKSet, null);
		
		assertTrue(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER.matches(keysToAdd.get(0)));
		
		assertEquals(1, keysToAdd.size());
	}
	
	
	@Test
	public void opGenerateMissingPermanentKeys_missingRefreshTokenKey()
		throws Exception {
		
		var incompleteJWKSet = new JWKSet(Arrays.asList(
			JWKsSpec.OP.HMAC.generateKey(),
			JWKsSpec.OP.SubjectEncryption.generateKey()
		));
		
		List<JWK> keysToAdd = JWKsGenerator.OP.generateMissingPermanentKeys(incompleteJWKSet, null);
		
		assertTrue(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER.matches(keysToAdd.get(0)));
		
		assertEquals(1, keysToAdd.size());
	}
	
	
	@Test
	public void opGenerateMissingPermanentKeys_missingAll()
		throws Exception {
		
		List<JWK> keysToAdd = JWKsGenerator.OP.generateMissingPermanentKeys(new JWKSet(), null);
		
		assertTrue(JWKsSpec.OP.HMAC.KEY_MATCHER.matches(keysToAdd.get(0)));
		assertTrue(JWKsSpec.OP.SubjectEncryption.KEY_MATCHER.matches(keysToAdd.get(1)));
		assertTrue(JWKsSpec.OP.RefreshTokenEncryption.KEY_MATCHER.matches(keysToAdd.get(2)));
		
		assertEquals(3, keysToAdd.size());
	}
	
	
	@Test
	public void opParseSample() throws IOException, ParseException, JOSEException {
		
		JWKSet jwkSet = JWKSet.load(new File("jwkSet.json"));
		
		JWKsValidator.OP.validatePresenceOfMinimumRequiredKeys(jwkSet);
	}


	// Federation

	@Test
	public void federationGenerate()
		throws Exception {

		final Consumer<String> eventMessageSink = new Consumer<>() {
			private final NumberedEventPrinter printer = new NumberedEventPrinter();

			@Override
			public void accept(final String eventMessage) {
				printer.print(eventMessage);
			}
		};

		JWKSet jwkSet = JWKsGenerator.Federation.generate(eventMessageSink);

		JWKsValidator.Federation.validatePresenceOfMinimumRequiredKeys(jwkSet);

		Iterator<JWK> jwkIterator = jwkSet.getKeys().iterator();

		JWK jwk = jwkIterator.next();
		assertTrue(jwk instanceof RSAKey);
		assertTrue(jwk.isPrivate());
		assertEquals(4, jwk.getKeyID().length());
		assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
		assertEquals(2048, jwk.size());
		assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
		assertNull(jwk.getKeyRevocation());

		assertEquals(1, jwkSet.size());
	}


	@Test
	public void federationAddNewKeys()
		throws Exception {

		JWKSet oldJWKSet = JWKsGenerator.Federation.generate(null);

		final Consumer<String> eventMessageSink = new Consumer<>() {
			private final NumberedEventPrinter printer = new NumberedEventPrinter();

			@Override
			public void accept(final String eventMessage) {
				printer.print(eventMessage);
			}
		};

		JWKSet newJWKSet = JWKsGenerator.Federation.generateAndPrefixNewRotatingKeys(oldJWKSet, eventMessageSink);

		JWKsValidator.Federation.validatePresenceOfMinimumRequiredKeys(newJWKSet);

		// Check that the new keys have been prepended
		KeyIDs merged = new KeyIDs(newJWKSet);
		merged.addAll(new KeyIDs(newJWKSet));
		assertEquals(merged, new KeyIDs(newJWKSet));

		Iterator<JWK> jwkIterator = newJWKSet.getKeys().iterator();

		JWK jwk = jwkIterator.next();
		assertTrue(jwk instanceof RSAKey);
		assertTrue(jwk.isPrivate());
		assertEquals(4, jwk.getKeyID().length());
		assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
		assertEquals(2048, jwk.size());
		assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
		assertNull(jwk.getKeyRevocation());

		jwk = jwkIterator.next();
		assertTrue(jwk instanceof RSAKey);
		assertTrue(jwk.isPrivate());
		assertEquals(4, jwk.getKeyID().length());
		assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
		assertEquals(2048, jwk.size());
		assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
		assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
		assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

		assertEquals(2, newJWKSet.size());
	}


	@Test
	public void federationAddNewKeys_rsa4096()
		throws Exception {

		JWKSet oldJWKSet = JWKsGenerator.Federation.generate(null);

		final Consumer<String> eventMessageSink = new Consumer<>() {
			private final NumberedEventPrinter printer = new NumberedEventPrinter();

			@Override
			public void accept(final String eventMessage) {
				printer.print(eventMessage);
			}
		};

		JWKSet newJWKSet = JWKsGenerator.Federation.generateAndPrefixNewRotatingKeys(oldJWKSet, 4096, eventMessageSink);

		JWKsValidator.Federation.validatePresenceOfMinimumRequiredKeys(newJWKSet);

		// Check that the new keys have been prepended
		KeyIDs merged = new KeyIDs(newJWKSet);
		merged.addAll(new KeyIDs(newJWKSet));
		assertEquals(merged, new KeyIDs(newJWKSet));

		Iterator<JWK> jwkIterator = newJWKSet.getKeys().iterator();

		JWK jwk = jwkIterator.next();
		assertTrue(jwk instanceof RSAKey);
		assertTrue(jwk.isPrivate());
		assertEquals(4, jwk.getKeyID().length());
		assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
		assertEquals(4096, jwk.size());
		assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
		assertNull(jwk.getKeyRevocation());

		jwk = jwkIterator.next();
		assertTrue(jwk instanceof RSAKey);
		assertTrue(jwk.isPrivate());
		assertEquals(4, jwk.getKeyID().length());
		assertEquals(KeyUse.SIGNATURE, jwk.getKeyUse());
		assertEquals(2048, jwk.size());
		assertTrue(isAroundCurrentTime(jwk.getIssueTime()));
		assertTrue(isAroundCurrentTime(jwk.getKeyRevocation().getRevocationTime()));
		assertEquals(KeyRevocation.Reason.SUPERSEDED, jwk.getKeyRevocation().getReason());

		assertEquals(2, newJWKSet.size());
	}


	@Test
	public void keyStoreGenerate() throws JOSEException {

		final Consumer<String> eventMessageSink = new Consumer<>() {
			private final NumberedEventPrinter printer = new NumberedEventPrinter();

			@Override
			public void accept(final String eventMessage) {
				printer.print(eventMessage);
			}
		};

		var encJWK = (OctetSequenceKey) JWKsGenerator.KeyStore.generate(eventMessageSink);

		assertEquals(128, encJWK.size());
		assertEquals(KeyUse.ENCRYPTION, encJWK.getKeyUse());
		assertEquals(4, encJWK.getKeyID().length());
		assertTrue(isAroundCurrentTime(encJWK.getIssueTime()));

		SecretKey aesKey = JWKsSpec.KeyStore.loadKey(encJWK);
		assertEquals("AES", aesKey.getAlgorithm());
		assertEquals(128, ByteUtils.bitLength(aesKey.getEncoded()));

		JWKsSpec.KeyStore.KEY_MATCHER.matches(encJWK);
	}
}
