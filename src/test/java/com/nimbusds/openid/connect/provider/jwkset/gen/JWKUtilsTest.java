package com.nimbusds.openid.connect.provider.jwkset.gen;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.*;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class JWKUtilsTest {


        @Test
        public void createPurposeMatcher_RSA() throws JOSEException {

                RSAKey jwk = JWKsSpec.OP.RotatedRSASigning.generateKey("1");

                JWKMatcher purposeMatcher = JWKUtils.createPurposeMatcher(jwk);

                assertEquals("kty=RSA use=sig with_id_only=true private_only=true", purposeMatcher.toString());
        }


        @Test
        public void createPurposeMatcher_EC_P256() throws JOSEException {

                ECKey jwk = JWKsSpec.OP.RotatedECSigning.generateKey(Curve.P_256, "1");

                JWKMatcher purposeMatcher = JWKUtils.createPurposeMatcher(jwk);

                assertEquals("kty=EC use=sig with_id_only=true private_only=true size=256 crv=P-256", purposeMatcher.toString());
        }


        @Test
        public void createPurposeMatcher_EdDSA() throws JOSEException {

                OctetKeyPair jwk = JWKsSpec.OP.RotatedEdDSASigning.generateKey("1");

                JWKMatcher purposeMatcher = JWKUtils.createPurposeMatcher(jwk);

                assertEquals("kty=OKP use=sig with_id_only=true private_only=true size=256 crv=Ed25519", purposeMatcher.toString());
        }
}
