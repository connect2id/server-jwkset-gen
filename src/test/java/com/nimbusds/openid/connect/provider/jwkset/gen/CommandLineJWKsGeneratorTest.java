package com.nimbusds.openid.connect.provider.jwkset.gen;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.util.Base64URL;
import com.nimbusds.jose.util.ByteUtils;
import com.nimbusds.openid.connect.provider.jwkset.JWKsSpec;
import org.junit.Test;

import javax.crypto.SecretKey;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;

import static org.junit.Assert.assertEquals;


public class CommandLineJWKsGeneratorTest {


        @Test
        public void main_noArgs() {

                PrintStream originalErr = System.err;
                ByteArrayOutputStream errContent = new ByteArrayOutputStream();

                System.setErr(new PrintStream(errContent));

                CommandLineJWKsGenerator.main(new String[]{});

                assertEquals("Missing required arguments\n", errContent.toString());

                System.setErr(originalErr);
        }


        @Test
        public void main_oneArg() {

                PrintStream originalErr = System.err;
                ByteArrayOutputStream errContent = new ByteArrayOutputStream();

                System.setErr(new PrintStream(errContent));

                CommandLineJWKsGenerator.main(new String[]{"key-store"});

                assertEquals("Missing required arguments\n", errContent.toString());

                System.setErr(originalErr);
        }


        @Test
        public void main_twoArgs_unknownContext() {

                PrintStream originalErr = System.err;
                ByteArrayOutputStream errContent = new ByteArrayOutputStream();

                System.setErr(new PrintStream(errContent));

                CommandLineJWKsGenerator.main(new String[]{"xxx", "target/jwkSet.json"});

                assertEquals("Unknown context: xxx\n", errContent.toString());

                System.setErr(originalErr);
        }


        @Test
        public void main_op_illegalRSAKeyBitSize_notAnInteger() {

                PrintStream originalErr = System.err;
                ByteArrayOutputStream errContent = new ByteArrayOutputStream();

                System.setErr(new PrintStream(errContent));

                CommandLineJWKsGenerator.main(new String[]{"op", "target/jwkSet.json", "-rsa", "aaa"});

                assertEquals("RSA key bit size must be one of [2048, 3072, 4096]\n", errContent.toString());

                System.setErr(originalErr);
        }


        @Test
        public void main_op_illegalRSAKeyBitSize_notAllowed() {

                PrintStream originalErr = System.err;
                ByteArrayOutputStream errContent = new ByteArrayOutputStream();

                System.setErr(new PrintStream(errContent));

                CommandLineJWKsGenerator.main(new String[]{"op", "target/jwkSet.json", "-rsa", "1024"});

                assertEquals("RSA key bit size must be one of [2048, 3072, 4096]\n", errContent.toString());

                System.setErr(originalErr);
        }


        @Test
        public void main_keyStore() throws IOException, ParseException, JOSEException {

                CommandLineJWKsGenerator.main(new String[]{"key-store", "target/jwk.json"});

                String content = Files.readString(Path.of("target", "jwk.json"));

                JWK jwk = JWK.parse(content);

                SecretKey secretKey = JWKsSpec.KeyStore.loadKey(jwk);

                assertEquals("AES", secretKey.getAlgorithm());
                assertEquals(ByteUtils.byteLength(128), secretKey.getEncoded().length);
        }


        @Test
        public void main_keyStore_b64() throws IOException, ParseException, JOSEException {

                CommandLineJWKsGenerator.main(new String[]{"key-store", "-b64", "target/jwk.json"});

                String content = Files.readString(Path.of("target", "jwk.json"));

                JWK jwk = JWK.parse(new Base64URL(content).decodeToString());

                SecretKey secretKey = JWKsSpec.KeyStore.loadKey(jwk);

                assertEquals("AES", secretKey.getAlgorithm());
                assertEquals(ByteUtils.byteLength(128), secretKey.getEncoded().length);
        }
}