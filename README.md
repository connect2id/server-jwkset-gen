# JWKs generator for Connect2id server deployments


Use this command line tool to generate a new JSON Web Keys (JWKs) for a 
Connect2id server, or to generate and add a new set of rotated signing and 
encryption keys.

For more information:

<https://connect2id.com/products/server/docs/config/jwk-set>

The package is available on Maven Central (see below) and includes a 
[JWKSetSpec](https://www.javadoc.io/doc/com.nimbusds/c2id-server-jwkset/latest/com/nimbusds/openid/connect/provider/jwkset/JWKSetSpec.html) 
class with the exact specifications, generators and selectors for the various 
server key types.


## Supported Connect2id server versions

* version 6.x - 17.x


## Usage

Requires Java 11+. 


### Key store encryption JWK

Generate a new key store encryption AES JWK:
```
java -jar jwks-gen.jar key-store jwk.json
```

BASE64URL encode the generated JWK:
```
java -jar jwks-gen.jar key-store -b64 jwk.json.b64
```


### OpenID provider JWK set

Generate a new OpenID provider JWK set:
```
java -jar jwks-gen.jar op jwkSet.json
```

For a BASE64URL encoded JWK set:
```
java -jar jwks-gen.jar op -b64 jwkSet.json.b64
```

Generate a new set of rotating keys and prefix it to an existing JWK set. The 
new JWK set will be checked for having all required permanent keys and will 
automatically generate and append them if missing (when upgrading to a newer 
Connect2id server release requiring a new type of permanent key):
```
java -jar jwks-gen.jar op oldJWKSet.json newJWKSet.json
```


### OpenID Federation 1.0 entity JWK set

Not required unless the Connect2id server is enabled for OpenID Federation 1.0.

Generate a new federation entity JWK set:
```
java -jar jwks-gen.jar federation jwkSet.json
```

Generate a new rotating key and prefix it to an existing federation entity JWK 
set:
```
java -jar jwks-gen.jar federation oldJWKSet.json newJWKSet.json
```


## Executable JAR

### Build with Maven

You can build it yourself with the following Maven command:

```text
mvn package assembly:single
```

### Download

Alternatively, you can download a built executable JAR from the
[downloads](https://bitbucket.org/connect2id/server-jwks-gen/downloads)
section.


## Maven

Maven coordinates:

```xml
<dependency>
    <groupId>com.nimbusds</groupId>
    <artifactId>c2id-server-jwkset</artifactId>
    <version>[ version ]</version>
</dependency>
```

## Questions?

Get in touch with [Connect2id support](https://connect2id.com/contact).


2025-02-26